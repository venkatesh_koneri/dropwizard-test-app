package com.ioof.monocle.app.health;

import com.codahale.metrics.health.HealthCheck;

public class MonocleObject1HealthCheck extends HealthCheck {
	
	private final String attribute1;
	
	public MonocleObject1HealthCheck(String attribute1) {
		this.attribute1 = attribute1;
	}

	@Override
	protected Result check() throws Exception {
		final String attribute1Result = attribute1;
		if (!attribute1Result.contains("default")) {
            return Result.unhealthy("attribute1 doesn't include a default");
        }
        return Result.healthy();
	}

}
