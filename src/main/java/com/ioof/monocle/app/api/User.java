package com.ioof.monocle.app.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	@JsonProperty("phone_number")
	private String phoneNumber;
	
	public User() {}

	public User(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
