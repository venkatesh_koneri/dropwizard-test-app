package com.ioof.monocle.app.api;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;

public class MessageQueueFactory {
	
	@NotEmpty
    private String host;

    @Min(1)
    @Max(65535)
    private int port = 5672;

    @JsonProperty
	public String getHost() {
		return host;
	}

    @JsonProperty
	public void setHost(String host) {
		this.host = host;
	}

    @JsonProperty
	public int getPort() {
		return port;
	}

    @JsonProperty
	public void setPort(int port) {
		this.port = port;
	}
    
    public MessageQueueClient build(Environment environment) {
        MessageQueueClient client = new MessageQueueClient(getHost(), getPort());
        environment.lifecycle().manage(new Managed() {
            @Override
            public void start() {
            }

            @Override
            public void stop() {
                client.close();
            }
        });
        return client;
    }
}
