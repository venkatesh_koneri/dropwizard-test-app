package com.ioof.monocle.app.api;

public class MonocleObject1 {
	
	private long id;
	private String attribute1;
	
	public MonocleObject1() {
		
	}
	
	public MonocleObject1(long id, String attribute1) {
		this.id = id;
		this.attribute1 = attribute1;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	
}
