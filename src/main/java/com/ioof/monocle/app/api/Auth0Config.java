package com.ioof.monocle.app.api;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Auth0Config {
	
	@NotEmpty
	private String domain;
	
	@NotEmpty
	private String client_id;
	
	@NotEmpty
	private String client_secret;
	
	@NotEmpty
	private String send;
	
	@NotEmpty
	private String connection;
	
	public String getConnection() {
		return connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	@JsonProperty
	public String getSend() {
		return send;
	}

	@JsonProperty
	public void setSend(String send) {
		this.send = send;
	}

	@JsonProperty
	public String getDomain() {
		return domain;
	}
	
	@JsonProperty
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@JsonProperty
	public String getClient_id() {
		return client_id;
	}
	
	@JsonProperty
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	@JsonProperty
	public String getClient_secret() {
		return client_secret;
	}

	@JsonProperty
	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

}
