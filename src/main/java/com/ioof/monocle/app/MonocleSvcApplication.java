package com.ioof.monocle.app;

import javax.ws.rs.client.Client;

import com.ioof.monocle.app.api.MessageQueueClient;
import com.ioof.monocle.app.health.MonocleObject1HealthCheck;
import com.ioof.monocle.app.resources.MonocleIdentityResource;
import com.ioof.monocle.app.resources.MonocleObject1Resource;

import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MonocleSvcApplication extends Application<MonocleSvcAppConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MonocleSvcApplication().run(args);
    }

    @Override
    public String getName() {
        return "monocle";
    }

    @Override
    public void initialize(final Bootstrap<MonocleSvcAppConfiguration> bootstrap) {
    	bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                                                   new EnvironmentVariableSubstitutor(false)
                )
        );
    }

    @Override
    public void run(final MonocleSvcAppConfiguration configuration,
                    final Environment environment) {
    	
    	MessageQueueClient messageQClient = configuration.getMessageQueueFactory().build(environment);
    	
    	final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
                .build(getName());
    	
    	final MonocleObject1Resource resource = new MonocleObject1Resource(
    	        configuration.getEnv_test()
    	    );
    	final MonocleIdentityResource identityResource = new MonocleIdentityResource(
    	        configuration.getAuth0(), client
    	    );
    	
    	final MonocleObject1HealthCheck healthCheck = new MonocleObject1HealthCheck(
    			configuration.getAttribute1()
    	);
    	
    	
    	
    	environment.healthChecks().register("default", healthCheck);
    	environment.jersey().register(resource);
    	environment.jersey().register(identityResource);
    }

}
