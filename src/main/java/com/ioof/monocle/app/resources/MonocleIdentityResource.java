package com.ioof.monocle.app.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.ioof.monocle.app.api.Auth0Config;
import com.ioof.monocle.app.api.User;

@Path("/auth")
public class MonocleIdentityResource {

	private Auth0Config auth0Config;
	private Client client;

	public MonocleIdentityResource(Auth0Config auth0Config, Client client) {
		super();
		this.client = client;
		this.auth0Config = auth0Config;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/signup")
	public User signup(User user) {
		Form form = new Form();
		form.param("phone_number", user.getPhoneNumber());
		form.param("client_id", auth0Config.getClient_id());
		form.param("client_secret", auth0Config.getClient_secret());
		form.param("connection", auth0Config.getConnection());
		form.param("send", auth0Config.getSend());



		Response response = client.target(auth0Config.getDomain()).request()
				.post(Entity.entity(form, MediaType.APPLICATION_JSON_TYPE));
		
		System.out.println(response.readEntity(String.class));
		return user;

	}

}
