package com.ioof.monocle.app.resources;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.ioof.monocle.app.api.MonocleObject1;
import com.ioof.monocle.app.core.PhoneNumberValidation;

@Path("/monocle-object")
@Produces(MediaType.APPLICATION_JSON)
public class MonocleObject1Resource {
	
	private final AtomicLong id;
	private final String attr;
	
	public MonocleObject1Resource(String attribute1) {
		this.id = new AtomicLong();
		this.attr = attribute1;
	}
	
	@GET
    @Timed
	public MonocleObject1 getMonocleObject(@Valid @PhoneNumberValidation @QueryParam("attribute1") Optional<String> attr1) {
		return new MonocleObject1(id.incrementAndGet(), this.attr + " Message");
	}

}
