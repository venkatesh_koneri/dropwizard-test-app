package com.ioof.monocle.app;

import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ioof.monocle.app.api.Auth0Config;
import com.ioof.monocle.app.api.MessageQueueFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class MonocleSvcAppConfiguration extends Configuration {
    // TODO: implement service configuration
	@NotEmpty
    private String attribute1;
	
	@NotEmpty
	private String env_test;
	 
	private Auth0Config auth0;
	
	@Valid
	@NotNull
	private MessageQueueFactory messageQueue = new MessageQueueFactory();
	
	@Valid
    @NotNull
    private JerseyClientConfiguration jerseyClient = new JerseyClientConfiguration();
	

    @JsonProperty("messageQueue")
    public MessageQueueFactory getMessageQueueFactory() {
        return messageQueue;
    }

    @JsonProperty("messageQueue")
    public void setMessageQueueFactory(MessageQueueFactory factory) {
        this.messageQueue = factory;
    }

	
	@JsonProperty
	public String getAttribute1() {
		return attribute1;
	}
	
	@JsonProperty
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	
	@JsonProperty
	public Auth0Config getAuth0() {
		return auth0;
	}

	@JsonProperty
	public void setAuth0(Auth0Config auth0) {
		this.auth0 = auth0;
	}

	@JsonProperty("jerseyClient")
    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return jerseyClient;
    }

    @JsonProperty("jerseyClient")
    public void setJerseyClientConfiguration(JerseyClientConfiguration jerseyClient) {
        this.jerseyClient = jerseyClient;
    }

    @JsonProperty
	public String getEnv_test() {
		return env_test;
	}

    @JsonProperty
	public void setEnv_test(String env_test) {
		this.env_test = env_test;
	}
	
}
