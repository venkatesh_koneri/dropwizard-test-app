package com.ioof.monocle.app.core;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumberValidation,Optional<String>>{

	@Override
	public boolean isValid(Optional<String> value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		
		Pattern pattern = Pattern.compile("(^\\+)\\d{11}$");
		Matcher matcher = pattern.matcher(value.get());
		System.out.println(value.get());
		System.out.println(matcher.matches());
		if(matcher.matches()) {
			return true;
		}
		return false;
	}

}
