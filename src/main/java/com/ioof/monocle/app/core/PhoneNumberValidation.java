package com.ioof.monocle.app.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {PhoneNumberValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface PhoneNumberValidation {
	
	String message() default "{Invalid phone number}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
